This repository is about the Arduino code to get 12V from a Qualcomm Quickcharge 3.0 adapter or powerbank for the RIPR.

The following library has been used: https://github.com/vdeconinck/QC3Control
See the README of that library for more explanation.

De following code is used as the Arduino code:

```C
#include <QC3Control.h>

//Pin 0 for Data+
//Pin 1 for Data-Dm
//Pin 2 for Data-DmGnd

QC3Control quickCharge(0, 1, 2);

void setup() {
  //Optional
  quickCharge.begin();
                                              
  //set voltage to 12V
  quickCharge.set12V();
  delay(1000);
}

void loop() {

}
```